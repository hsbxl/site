# HSBXL Site

This site is built using the [Hugo static site generator](https://gohugo.io/).

## Instructions: Adding New Content

### Adding an Event

1. Create a folder within `/content/events`.
2. Inside this folder, create a markdown file for your event.
   - Example: `/content/events/my_awesome_workshop.md`
   - Refer to [this example](https://gitlab.com/hsbxl/agenda/raw/master/content/events/panpanpan.md) for guidance.

### Adding a Project

1. Create a folder within `/content/projects`.
2. Inside this folder, create a markdown file for your project.
   - Example: `/content/projects/my_awesome_project.md`
   - Refer to [this example](https://gitlab.com/hsbxl/agenda/raw/master/content/projects/the_black_knight.md) for guidance.

## Instructions: Adding Page Features

### Displaying an Event List

To show a list of events with a specific series, use the following shortcode:

```html
{{< events series="foobar" >}}
```

On an event page, add `series: foobar` at the top to categorize it under the 'foobar' series. This will allow the event to be grouped with other events in the same series when using the event list shortcode.

### Adding a Single Image

1. Create a folder in the event or project directory.
2. Place your image in this folder.
3. Add the following shortcode to your event or project markdown file:

```html
{{< image src="images/foobar.jpg" >}}
```

To resize the image to a specific width (e.g., 200 pixels), use the `width` attribute:

```html
{{< image src="images/foobar.jpg" width="200x" >}}
```

If you want to link the image to a URL, use the `link` attribute:

```html
{{< image src="images/foobar.jpg" link="http://example.com" >}}
```

This will make the image clickable, directing users to the specified URL when they click on it.

If you want to create a more complex interaction, such as linking the image to another page on your site or an external resource, simply replace the `link` attribute's value with the desired URL. This is particularly useful for highlighting specific content, guiding users to additional resources, or creating call-to-action elements within your event or project pages.

For instance, to link the image to another page on your site:

```html
{{< image src="images/foobar.jpg" link="/another-page" >}}
```

Or, to link it to an external website:

```html
{{< image src="images/foobar.jpg" link="https://example.com" >}}
```

This will make the image clickable, directing users to the specified external website when they click on it. This is particularly useful when you want to provide users with quick access to related content, resources, or external references that complement the information on your event or project page.

For example, you could link an image to a partner organization's website, a relevant article, or any other external resource that adds value to the content you're presenting.

Make sure the image file (`foobar.jpg`) is correctly located in the `images` directory within the appropriate event or project folder to ensure proper loading and functionality.

### Adding an Image Gallery

1. Create a folder within your event/project directory and add your images to this folder.
2. Use the following shortcode in your markdown file where you want the gallery to appear:

```html
{{< gallery "image_directory_name" >}}
```

Replace `image_directory_name` with the actual name of the directory containing your images. This directory should be located within the specific event or project folder where you want the gallery to appear. The gallery shortcode will automatically display all the images found in this directory, creating an interactive gallery that users can navigate through.

For example, if you have a folder named `event_images` within your event directory, the shortcode would look like this:

```html
{{< gallery "event_images" >}}
```

This will render a gallery of all images stored in the `event_images` folder on the page where you've placed the shortcode. Users will be able to browse through the images directly on the event or project page, making it easy to display multiple images in an organized and visually appealing manner.

The image gallery is particularly useful for showcasing photos from events, detailed project images, or any collection of visuals that enhance the content of your page. By using this shortcode, you ensure that your gallery is responsive and integrates seamlessly with the rest of your site's design.

### Embedding YouTube Videos

To embed a YouTube video into your event or project page, use the following shortcode:

```html
{{< youtube oHg5SJYRHA0 >}}
```

Replace `oHg5SJYRHA0` with the YouTube video ID, which is the unique string of characters found at the end of the video’s URL. For example, in the URL `https://www.youtube.com/watch?v=oHg5SJYRHA0`, the video ID is `oHg5SJYRHA0`.

This shortcode will embed the video directly onto your page, allowing users to watch it without leaving your site. Embedding videos is an excellent way to include multimedia content, such as event highlights, tutorials, promotional videos, or any other video content that adds value to your page.

By using the YouTube shortcode, you ensure that the video is properly formatted and responsive, fitting well within your site's layout across different devices.

For more detailed information and additional shortcode options, you can refer to the [Hugo Documentation on Shortcodes](https://gohugo.io/content-management/shortcodes/#youtube).

### Embedding PeerTube Videos

**Source:** [GitHub by bugsysop](https://github.com/bugsysop/hugo-peertube-shortcode/)

Insert the _Shortcode_ in your Markdown content...

**Simplified syntax**
Easy to use, but only default options.

```
{{< peertube videos.trom.tf a547c41d-3f0e-4689-bb1c-44d533d16397 >}}
```

**Complete syntax**  
Give access to the complete set of options avaible for Peertube iframe.

```
{{< peertube host="videos.trom.tf" id="a11de1b8-dbb2-4cef-9b1d-3f01e0af8425" title="0" >}}
```

| Params     | Comments                                                                        | Default |
| ---------- | ------------------------------------------------------------------------------- | ------- |
| `host`     | _Domain Name_ of instance: without `https://` nor training slatch (`/`)         | `None`  |
| `id`       | Identification _Code_ of the video (ex: `d49f95a9-b183-4f16-9341-8637ac3597ff`) | `None`  |
| `title`    | Display the video _Title_: `0` (no) or `1` (yes)                                | `0`     |
| `warning`  | Display a _Warning_ about privacy: `0` (no) or `1` (yes)                        | `0`     |
| `subtitle` | Display the video subtitle if exist: langage code (ex: `fr`)                    | `0`     |

Note: The default options are _hard coded_, if you want du change, you have to modify the source.

### Privacy

**1)** Peertube by itself use absolutely no cookies, no trackers.

**2)** The uses the BitTorrent P2P protocol to share bandwidth between users by default to help lower the load on the server induce that your IP address is being stored in the instance’s BitTorrent tracker as long as you download or watch the video For [more info](https://indymotion.fr/about/peertube#privacy) read this Peertube statement. 


## License

This project is licensed under the [MIT License](LICENSE), as it was originally a [fork](https://gitlab.com/hsbxl/hsbxl-website/commit/b7674cac2be261e030170f4b9de7c653847543af) of the [Hugo Code Editor Theme](https://github.com/aubm/hugo-code-editor-theme), which also uses the [MIT License](https://github.com/aubm/hugo-code-editor-theme/blob/4217a2dd1b3b621898835f649e62cf736d5ac9d2/LICENSE.md).
