import fs from "node:fs";
import matter from "gray-matter";
import moment from "moment";

// Function to display help message with script purpose and usage instructions
const showHelp = () => {
  console.log(`
    Purpose:
    This script (generate-events.js) is used to update events in the calendar for the Brussels HackerSpace (HSBXL) website.
    It increments the event number and date in the markdown files of events.

    Usage: 
    node generate-events.js <path-to-markdown-file> <number-of-files>
    
    - path-to-markdown-file: Relative or absolute path to the markdown file.
    - number-of-files: Number of markdown files to generate.

    Example: 
    node generate-events.js content/events/techtuesday/712.md 5
    `);
};

// Function to read and parse the markdown file
const readAndParseMarkdown = (filePath) => {
  try {
    const content = fs.readFileSync(filePath, "utf8");
    return matter(content);
  } catch (error) {
    console.error("Error reading the markdown file:", error);
    process.exit(1);
  }
};

// Function to validate the data types of specific fields
const validateFields = (data) => {
  const fieldsToValidate = ["starttime", "endtime"];
  fieldsToValidate.forEach((field) => {
    if (data[field] && typeof data[field] !== "string") {
      console.error(
        `Error: The "${field}" field is not a string. Please check the markdown file.`,
      );
      process.exit(1);
    }
  });
};

// Function to update fields containing the previous event number, while ignoring certain fields
const updateEventNumberInData = (data, previousEventNumber, newEventNumber) => {
  const fieldsToIgnore = ["starttime", "endtime"];
  const regex = new RegExp(`(?<!\\S)${previousEventNumber}(?!\\S)`, "g");

  for (const key in data) {
    if (typeof data[key] === "string" && !fieldsToIgnore.includes(key)) {
      data[key] = data[key].replace(regex, newEventNumber);
    }
  }
};

// Function to create a new markdown file for the next event
const createNextEventMarkdown = (
  parsedMarkdown,
  filePath,
  previousEventNumber,
  newEventNumber,
  startDate,
) => {
  const nextEventDate = startDate.add(1, "weeks").format("YYYY-MM-DD");

  // Update the start date and event number in all relevant fields, carefully avoiding certain fields
  parsedMarkdown.data.startdate = nextEventDate;
  updateEventNumberInData(
    parsedMarkdown.data,
    previousEventNumber,
    newEventNumber,
  );

  // Convert the updated front matter and content back to markdown
  const newMarkdownContent = matter.stringify(
    parsedMarkdown.content,
    parsedMarkdown.data,
  );

  fs.writeFileSync(filePath, newMarkdownContent, "utf8");
  console.log(
    `New markdown file created for event #${newEventNumber} on ${nextEventDate}`,
  );
};

const main = () => {
  const args = process.argv.slice(2);

  if (args.length !== 2 || isNaN(parseInt(args[1], 10))) {
    showHelp();
    return;
  }

  const markdownPath = args[0];
  const numberOfFiles = parseInt(args[1], 10);
  const originalEventNumber = parseInt(markdownPath.match(/\d+/)[0], 10);
  let parsedMarkdown = readAndParseMarkdown(markdownPath);

  // Validate specific fields
  validateFields(parsedMarkdown.data);

  if (parsedMarkdown) {
    let startDate = moment(parsedMarkdown.data.startdate, "YYYY-MM-DD");

    for (let i = 1; i <= numberOfFiles; i++) {
      const newEventNumber = originalEventNumber + i;
      const newMarkdownPath = markdownPath.replace(
        /\d+\.md$/,
        `${newEventNumber}.md`,
      );

      createNextEventMarkdown(
        parsedMarkdown,
        newMarkdownPath,
        originalEventNumber + i - 1,
        newEventNumber,
        startDate,
      );
      startDate = moment(parsedMarkdown.data.startdate, "YYYY-MM-DD");
    }
  }
};

main();
