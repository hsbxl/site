---
title: "Contact us"
linktitle: "Contact us"
---

## Primary ways of reaching us:

## WE HAVE MOVED !!

- **Address:** Rue osseghem 53, 1080 Sint-Jans Molenbeek, Brussels, Belgium.
- **Pidgeon:** 50.85516,4.32111
  ([Open Street Map](https://www.openstreetmap.org/node/7115056369))
  ([GoogleMaps](https://www.google.com/maps/place/Hackerspace+Brussels+HSBXL/@50.8552895,4.3188495,17z/data=!3m1!4b1!4m6!3m5!1s0x47c3c37ae6875a27:0x962d7fc36e046fa7!8m2!3d50.8552861!4d4.3214244!16s%2Fg%2F12lk5rsf3?entry=ttu))
- **Matrix chat:** \#hsbxl on hackerspaces.be ([webchat](https://matrix.to/#/#hsbxl:matrix.org)) ([Join Matrix Space](https://matrix.to/#/#hackerspace-brussels:matrix.org))
- **Email:** [contact@hsbxl.be](mailto:contact@hsbxl.be)
- **Telephone:** [+32 2 880 40 04](tel:003228804004) (phone is in space, not always answered)

## Other ways of reaching us:

- **Mastodon/Fediverse:** [@hsbxl@mastodon.online](https://mastodon.online/@HSBXL)
- **EventPhone VPN Telephone:** [1080](tel:1080) (https://eventphone.de/doku/epvpn)
- **Facebook:** [HSBXL on Facebook](https://www.facebook.com/groups/hsbxl/)
- **Twitter:** [hsbxl](http://twitter.com/hsbxl) and [\#hsbxl](https://twitter.com/search?q=%23hsbxl)
- **Mailing-list:** see the [ mailing-List](/mailinglist) page
- **Bank:** Argenta IBAN: BE69 9794 2977 5578; BIC: ARSP BE 22
- **Youtube:** https://www.youtube.com/@hackerspacebrussels5206

If you are trying to get to HSBXL, please look [here](https://hsbxl.be/enter/) for a detailed description.

## Statutes at Belgisch Staatsblad / Moniteur Belge

- **Trade number**: BE0817617057
- **Official link to all statutes for HSBXL**: [BCE/KBO](https://kbopub.economie.fgov.be/kbopub/zoeknummerform.html?lang=en&nummer=0817617057)
- **2009-08-06**: [Statutes: Rubric Constitution (New Juridical Person, Opening Branch, etc...)](https://www.ejustice.just.fgov.be/tsv_pdf/2009/08/18/09118276.pdf)
- **2013-08-12**: [Statutes: Designation - Registered Office - Resignations - Appointments](https://www.ejustice.just.fgov.be/tsv_pdf/2013/08/22/13131033.pdf)
- **2016-05-10**: [Statutes: Registered Office - Resignations - Appointments](https://www.ejustice.just.fgov.be/tsv_pdf/2016/05/20/16069254.pdf)
- **2019-07-17**: [Statutes: Registered Office - Resignations - Appointments](https://www.ejustice.just.fgov.be/tsv_pdf/2019/07/24/19099868.pdf)
- **2022-01-10**: [Statutes: Resignations - Appointments](http://www.ejustice.just.fgov.be/tsv_pdf/2022/01/17/22007203.pdf)
