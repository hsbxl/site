---
startdate: 2020-01-29
starttime: "19:00"
enddate: 2020-02-01
#endtime: "05h"
allday: true
linktitle: "NibbleWeek 2020"
title: "NibbleWeek 2020"
location: "HSBXL"
eventtype: "Four bits of awesomeness"
price: ""
image: "byteweek.png"
series: "byteweek2020"
start: "true"
---

The week before Fosdem conference, HSBXL compiles NibbleWeek.  
Four 'Bitdays' of hackatons, workshops and talks.  
We end our week with the notorious [Bytenight](/bytenight)!

# Wednesday January 29th

{{< events series="byteweek2020" when="2020-01-29" showtime="true" showallday="true" >}}

# Thursday January 30th

{{< events series="byteweek2020" when="2020-01-30" showtime="true" showallday="true" >}}

# Friday January 31st

{{< events series="byteweek2020" when="2020-01-31" showtime="true" showallday="true" >}}

# Saturday February 1st

{{< events series="byteweek2020" when="2020-02-01" showtime="true" showallday="true" >}}

# Call for participation

If you want to do a talk, a workshop or have a hackaton,  
send a mail to **contact@hsbxl.be** with your proposal.
