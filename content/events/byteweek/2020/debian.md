---
startdate: 2020-01-29
starttime: "8:00"
enddate: 2020-01-31
endtime: "18:00"
allday: true
linktitle: "MiniDebCamp"
title: "MiniDebCamp"
location: "HSBXL"
eventtype: "Workshop/hackathon"
price: "Free"
series: "byteweek2020"
image: "debian_logo.svg"
---

## Basics

MiniDebCamp is some days of hacking on Debian and related stuff. No orga, no overhead, we just meet somewhere nice and interesting, and hack hack hack.

This one is taking place the three days before https://fosdem.org/2020/ at the Hackerspace Brussels (BSHXL).

Main website and registration: https://wiki.debian.org/DebianEvents/be/2020/MiniDebCamp

Coordinator: holger@debian.org

## Organisation

BYOD and do your own organisation! Everything is self organized and provided, except what the location provides (which includes network and drinks, but no food and no place to sleep and no stage for video coverage neither...).

As usual, we apply the [DebConf Code of Conduct](http://debconf.org/codeofconduct.shtml) and the [Debian Code of Conduct](https://www.debian.org/code_of_conduct) and aim to be excellent to each other!
