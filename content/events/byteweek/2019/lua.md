---
startdate: 2019-01-28
starttime: "10:00"
enddate: 2019-02-01
endtime: "18:00"
allday: true
linktitle: "Lua on a Stick"
title: "Lua on a stick"
location: "HSBXL"
eventtype: "Workshop/hackathon"
price: "Free"
series: "byteweek2019"
image: "rwr_nwn_lua-on-a-stick_2019.png"
---

# Overview

5 days of:

- Shining a spotlight on Lua's functionality
- Hacking through using Lua and affiliate toolset
- Presenting interesting tools informally
- Learning through osmosis
- Drinking moonshine

# Topic Areas

Areas of focus (may) include:

- Parsing large XML datasets using SAX parsers
- Regular expressions and parsing

# Volunteering

Calls for volunteering or presenting projects are most welcome

Please contact via:

- mail contact@hsbxl.be

# Supporting

Please support this event (and others) through promotion via our official social media pages

- [Meetup](https://www.meetup.com/hackerspace-Brussels-hsbxl/events/)
- [Twitter](https://twitter.com/luaonastick)

# SAT 2nd FEBRUARY, BYTENIGHT

We end our week with the notorious social all nighter, [Bytenight](https://hsbxl.be/events/byteweek/2019/bytenight/)!

# NEW: Fri 1st February, Bitnight

In addition to Saturday's [Bytenight](https://hsbxl.be/events/byteweek/2019/bytenight/), there will be a smaller social event on Friday, [Bitnight](https://hsbxl.be/events/byteweek/2019/bitnight/).

This will mix workshop and hackathon participants from that day with early FOSDEM visitors coming to Brussels.

All FOSS friends welcome

See Byteweek for details on HSBXL's pre-FOSDEM events:
[https://hsbxl.be/events/byteweek/2019/](https://hsbxl.be/events/byteweek/2019/)
