---
linktitle: "Byteweek"
title: "Byteweek"
location: "HSBXL"
eventtype: "Conference"
price: "Free"
---

The week before Fosdem conference, HSBXL compiles ByteWeek.  
Eight 'Bitdays' of hackatons, workshops and talks.  
We end our week with the notorious [Bytenight](/bytenight)!

## Upcoming Byteweek

{{< series when="upcoming" series="byteweek2023" >}}

## Past Byteweeks

{{< series when="past" series="byteweek2023" >}}
