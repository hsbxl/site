---
startdate: 2023-02-03
starttime: "09:00"
enddate: 2023-02-03
endtime: "19:00"
allday: true
linktitle: "pre-FOSDEM Byteweek slot"
title: "pre-FOSDEM Byteweek slot"
location: "HSBXL"
eventtype: "Hackaton"
price: "Free"
series: "byteweek2023"
---

# Currently planned for today

{{< events series="byteweek2023" when="2023-02-03" showtime="true" showallday="true" >}}

# Invite your FOSS group over!

Hackerspace Brussels is opening up for the FOSS community to hack on their projects in the advent of FOSDEM.
There's heating, food, WiFi and also our tool corner to get creative.
There's a setup to give presentations, do workshops or even stream to your community if you feel inclined to do so.
Reach out at contact@hsbxl.be for more details.
