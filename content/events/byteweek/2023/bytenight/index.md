---
startdate: 2023-02-04
starttime: "20:00"
linktitle: "Bytenight"
title: "Bytenight 2023: Finally again!"
location: "HSBXL"
eventtype: "Thé dansant"
price: "pay for your drinks please"
image: "bytenight.png"
series: "byteweek2023"
aliases: [/bytenight/]
---

As an after-party to the first day of the big FOSDEM Free Software conference in Brussels,  
the oldest Hackerspace in town organises its annual free party.

[CONTACT PAGE](https://hsbxl.be/contact/)
[getting to the location](https://hsbxl.be/enter/)

# Version 12 (0xc)

Let's throw a party like it's 1999... meh, the Y2K was overhyped... let's party like it's 2038!

# Music

Weird music for weird people.

## Lineup

These artists have confirmed to play a set for us:

- CedricHacksThePlanet
- Joe D’Absynth
- Meex
- A.I. Caramba featuring Taloor Soift
- ...

Jegeva also took care of fantastic lights!

# Drinks

- Club-Mate
- a collecton of speciality beers
- soft drinks
- "normal" beers

# Unsupported, deprecated versions

- [Bytenight v2020](https://hsbxl.be/events/byteweek/2020/bytenight/)
- [Bytenight v2019](https://hsbxl.be/events/byteweek/2019/bytenight/)
- [Bytenight v2018](https://wiki.hsbxl.be/Bytenight_2018)
- [Bytenight v2017](https://wiki.hsbxl.be/Bytenight_2017)
- [Bytenight v2016](<https://wiki.hsbxl.be/Bytenight_(2016)>)
- [Bytenight v2015](<https://wiki.hsbxl.be/Bytenight_(2015)>)
- [Bytenight v2014](<https://wiki.hsbxl.be/Bytenight_(2014)>)
- [Bytenight v2013](https://wiki.hsbxl.be/Bytenight_Bye_Bye_Garage)
- [Bytenight v2012](<https://wiki.hsbxl.be/ByteNight_(2012)>)
- [Bytenight v2011](<https://wiki.hsbxl.be/ByteNight_(2011)>)
- [Bytenight v2010](<https://wiki.hsbxl.be/ByteNight_(2010)>)

# Organizing

Notes can be found on https://etherpad.openstack.org/p/bytenight2020
)
