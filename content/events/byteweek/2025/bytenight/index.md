---
startdate: 2025-02-01
starttime: "20:00"
enddate: 2025-02-02
endtime: "4:00"
linktitle: "Bytenight"
title: "Bytenight 2025"
location: "HSBXL"
eventtype: "Thé dansant"
price: "pay for your drinks please"
series: "byteweek2025"
aliases: [/bytenight/]
---

# 🎉 ByteNight 2025 is coming up! 🎉

We are in the planning phase of the event, more information will follow.

# [EVENT WEBSITE](https://bytenight.brussels)

[CONTACT PAGE](https://hsbxl.be/contact/)
[getting to the location](https://hsbxl.be/enter/)

# Unsupported, deprecated versions

- [Bytenight v2024](https://hsbxl.be/events/byteweek/2024/bytenight/)
- [Bytenight v2023](https://hsbxl.be/events/byteweek/2023/bytenight/)
- [Bytenight v2020](https://hsbxl.be/events/byteweek/2020/bytenight/)
- [Bytenight v2019](https://hsbxl.be/events/byteweek/2019/bytenight/)
- [Bytenight v2018](https://wiki.hsbxl.be/Bytenight_2018)
- [Bytenight v2017](https://wiki.hsbxl.be/Bytenight_2017)
- [Bytenight v2016](<https://wiki.hsbxl.be/Bytenight_(2016)>)
- [Bytenight v2015](<https://wiki.hsbxl.be/Bytenight_(2015)>)
- [Bytenight v2014](<https://wiki.hsbxl.be/Bytenight_(2014)>)
- [Bytenight v2013](https://wiki.hsbxl.be/Bytenight_Bye_Bye_Garage)
- [Bytenight v2012](<https://wiki.hsbxl.be/ByteNight_(2012)>)
- [Bytenight v2011](<https://wiki.hsbxl.be/ByteNight_(2011)>)
- [Bytenight v2010](<https://wiki.hsbxl.be/ByteNight_(2010)>)
