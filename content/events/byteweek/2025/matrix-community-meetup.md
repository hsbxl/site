---
startdate: 2025-01-31
starttime: "13:00"
enddate: 2025-01-31
endtime: "21:00"
allday: true
linktitle: "Matrix Foundation & Community Meetup"
title: "Matrix Foundation & Community Meetup"
location: "HSBXL"
eventtype: "Barcamp"
price: "Free"
series: "byteweek2025"
image: "matrix-favicon-white.png"
---

# Matrix Foundation & Community Meetup

There is a large interest in FOSDEM from the Matrix community, and the Matrix.org Foundation and Community are organising this Pre-FOSDEM Barcamp in addition to the officially alotted time [for the Matrix track](https://fosdem.org/2025/schedule/track/matrix/) at FOSDEM.
Please join us in the event's Matrix room: [#fosdem-2025-barcamp:matrix.org](https://matrix.to/#/#fosdem-2025-barcamp:matrix.org).
Further info about Matrix at FOSDEM is available in the [blog post](https://matrix.org/blog/2024/11/matrix-fosdem-full-force/).

The Matrix.org Foundation thanks Hackerspace Brussels for the continued support through supplying the location for this event. ❤️
