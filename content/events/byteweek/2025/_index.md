---
startdate: 2025-01-27
enddate: 2025-02-01
allday: true
linktitle: "Byteweek 2025"
title: "Byteweek 2025"
location: "HSBXL"
eventtype: "5 days of awesomeness"
price: ""
image: "byteweek.png"
series: "byteweek2025"
start: "true"
aliases: [/byteweek]
---

The week before Fosdem conference, HSBXL compiles a week of hacking.  
bin(111) days of hackatons, workshops and talks.  
We end our week with the notorious [Bytenight](/bytenight)!

Invite your FOSS group over!
We are opening our doors for the FOSS community to hack on their projects in the advent of FOSDEM. There’s heating, food, WiFi and also our tool corner to get creative. There’s a setup to give presentations, do workshops or even stream to your community if you feel inclined to do so. Reach out at for more details!

# Monday January 27th

{{< events series="byteweek2025" when="2025-01-27" showtime="true" showallday="true" >}}

# Tuesday January 28th

{{< events series="byteweek2025" when="2025-01-28" showtime="true" showallday="true" >}}

# Wednesday January 29th

{{< events series="byteweek2025" when="2025-01-29" showtime="true" showallday="true" >}}

# Thursday January 30th

{{< events series="byteweek2025" when="2025-01-30" showtime="true" showallday="true" >}}

# Friday January 31st

{{< events series="byteweek2025" when="2025-01-31" showtime="true" showallday="true" >}}

# Saturday February 1st

{{< events series="byteweek2025" when="2025-02-01" showtime="true" showallday="true" >}}

# Sunday February 2nd

{{< events series="byteweek2025" when="2025-02-02" showtime="true" showallday="true" >}}

# Call for participation

If you want to do a talk, a workshop or have a hackathon,  
send a mail to **contact@hsbxl.be** with your proposal.
