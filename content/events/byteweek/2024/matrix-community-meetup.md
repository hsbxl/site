---
startdate: 2024-02-02
starttime: "13:00"
enddate: 2024-02-02
endtime: "21:00"
allday: true
linktitle: "Matrix Foundation & Community Meetup"
title: "Matrix Foundation & Community Meetup"
location: "HSBXL"
eventtype: "Barcamp"
price: "Free"
series: "byteweek2024"
image: "matrix-favicon-white.png"
---

# Matrix Foundation & Community Meetup

There is a large interest in FOSDEM from the Matrix community, and some folks have decided the officially alotted time [for the Matrix track](https://fosdem.org/2024/schedule/track/matrix/) at FOSDEM is not enough and are organizing this additional pre-FOSDEM meetup.
Find us at [#fosdem2024-foundation-community-meetup:matrix.org](https://matrix.to/#/#fosdem2024-foundation-community-meetup:matrix.org) for more info and to enter your suggestions what might happen.

Hackerspace Brussels has graciously offered to supply the location for this event.
There's heating, food, WiFi, and a setup to give presentations, do workshops, and more.
