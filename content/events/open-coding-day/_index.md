---
linktitle: "Open Coding Day Events"
title: "Open Coding Day Events"
location: "HSBXL"
eventtype: "co-working event"
price: "Free"
aliases: [/open-coding-day/]
series: "Open Coding Day"
start: "true"
---

Join us ** every friday ** from 9:30 for a full day of coding collaboration at HSBXL! This event is open to all who enjoy working in a community environment, whether you’re working on an open source or proprietary project. We invite you to bring your laptops, your energy, and your software projects to collaborate, learn, and share in the main room of our spacious hackerspace.

During this ** 7-hour co-working event ** , you’ll have the opportunity to work on your project, ask for feedback, offer help to others, and if you wish, share your achievements at the end of the day.

Please Note: While HSBXL has multiple rooms for various purposes, this co-working event is happening in the main room. It’s important to note that online meetings should be avoided during this event to prevent noise disturbance and to maintain a conducive work environment for everyone.

## Upcoming Open Coding Day Events

{{< events when="upcoming" series="Open Coding Day" >}}

## Past Open Coding Day Events

{{< events when="past" series="Open Coding Day" >}}
