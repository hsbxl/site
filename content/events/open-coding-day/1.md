---
linktitle: Open Coding Day 9
Eventid: OpenCodingDay10
series: Open Coding Day
title: "Open Coding Day: Come and Co-work on Your Projects"
location: HSBXL
eventtype: co-working event
startdate: "2023-07-13"
starttime: "10:00"
endtime: "18:00"
image: open-coding-day.png
---

## Open Coding Day: Come and Co-work on Your Projects

Join us for a full day of coding collaboration at HSBXL! This event is open to all who enjoy working in a community environment, whether you're working on an open source or proprietary project. We invite you to bring your laptops, your energy, and your software projects to collaborate, learn, and share in the main room of our spacious hackerspace.

During this **8-hour co-working event**, you'll have the opportunity to work on your project, ask for feedback, offer help to others, and if you wish, share your achievements at the end of the day.

**Please Note**: While HSBXL has multiple rooms for various purposes, this co-working event is happening in the main room. It's important to note that online meetings should be avoided during this event to prevent noise disturbance and to maintain a conducive work environment for everyone.

### What the day will look like:

1. Introduction and setting up: Find a comfortable spot in the main room, set up your workspace and grab a Club Mate, soft drink, or a coffee to get your day started.
2. Coding Session: Dive into your project, seek assistance, and collaborate with others.
3. Sharing Session: Optional, but encouraged! Share what you've worked on, your challenges, and triumphs.

### How to get to HSBXL:

HSBXL is located in Brussels. For detailed instructions on how to get to the hackerspace, please visit [here](https://hsbxl.be/enter/). If you encounter any issues while trying to get in, you can contact us at +32 28804004. Please note that the phone is inside the space. Alternatively, you can also reach out to us in our [Matrix chatroom](https://matrix.to/#/#hsbxl:matrix.org).

### Requirements

Please bring your laptop, and if you have, an extension cord. The space has WiFi, but having your own internet backup solution can also be useful.

Join us at this **8-hour co-working day** in the main room of HSBXL, and we hope you'll have made progress on your projects, learned something new, and most importantly, enjoyed the power of community collaboration. Whether you're an experienced developer, a newbie, or just someone interested in coding, we welcome you to this day of learning, sharing, and coding together.
