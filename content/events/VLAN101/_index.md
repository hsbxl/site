---
startdate: 2022-11-22
starttime: "18:30"
endtime: "19:30"
linktitle: "VLAN101 - entry into VLANs"
title: "VLAN101 - entry into VLANs"
price: "Free"
image: "vlan.jpeg"
series: "basics of networking"
eventtype: "Workshop"
location: "HSBXL"
---

## Language

This workshop will be given in English.

## Workshop

Many corporate networks spepperate traffic on ports inside a single switch with something called VLANs
This is one of those things that for network people is very basic , but for non-network people is sometimes oe of those "magic" things

we'll do a quick runtrough of the basic concepts and then play around a bit with multple networks and multiple switches in order to get to grips with VLANS

geared towards people who know little to nothing about networks.

## Requirements

- Your trusty laptop with an ethernet connection (either onboard or via dongle)
