---
startdate: 2019-03-31
starttime: "14:00"
endtime: "18:00"
linktitle: "Attack Delay Workshop"
title: "Attack Delay Workshop"
location: "HSBXL"
eventtype: "Chiptuning workshop"
price: "€40 for the complete kit"
image: "delay_workshop.jpg"
series: merda_elettronica
meetup: "https://www.meetup.com/hackerspace-Brussels-hsbxl/events/259088112"
facebook: "https://www.facebook.com/events/1147476125409724"
---

# Attack Delay Workshop

The workshop consists of the construction of a Delay effect circuit based on the classic PT2399
chip that many Delay commercial pedals use. The circuit has two potentiometers with which we
manage the time and feedback of the FX, they allow us to use the delay in a more conventional way
but also to take it to noisier terrain. The circuit will be assembled in a box to the liking of the participants and we will end the workshop by playing
the FX a bit and exploring its expressive possibilities within a jam of collective improvisation or
exploring more installation formats.
Registration 40 eu. (Includes complete kit with all materials). Bring soldering iron, tin, and
container to locate the circuit. No technical knowledge is required to carry out the workshop
successfully. Suitable for all

# If you can, bring

- soldering iron
- a box to mount the circuit
- some pots to connect to the delay in the final jam

# Important!

Seats are limited to 10 people.  
If you want to join, send a mail to ceneriera@gmail.com

# dinamizado por

- oscar martin [noish] http://noconventions.mobi/
- irma nex http://necrobiopsi.hotglue.me/
- jana jan https://janajan.tumblr.com/

# ln -s

- meetup: https://www.meetup.com/hackerspace-Brussels-hsbxl/events/259088112
- facebook: https://www.facebook.com/events/1147476125409724
