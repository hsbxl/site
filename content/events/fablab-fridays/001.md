---
eventid: FablabFriday001
startdate: 2019-08-02
starttime: "19:00"
linktitle: "FablabFriday001"
title: "FablabFriday001"
price: ""
image: "Fab_Lab_logo.png"
series: "FablabFridays"
eventtype: "Social get-together"
location: "HSBXL"
---

Every first Friday of the moth we are having a evening dedicated to building stuff

have you always wanted to learn how to work with the CNC machine  
Do you have a project or an item you would like to lasercut a box for?
would you like to design a widget and print in on the 3d Printer?

just drop on by!
