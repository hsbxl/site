---
linktitle: "Cryptoparty"
title: "Cryptoparty"
location: "HSBXL"
eventtype: "Cryptoparty"
series: "Cryptoparty"
start: "true"
aliases: [cryptoparty]
---

![cryptoparty](/images/cryptoparty.png)

# What?

CryptoParty is a decentralized movement with events happening all over the world. The goal is to pass on knowledge about protecting yourself in the digital space. This can include encrypted communication, preventing being tracked while browsing the web, and general security advice regarding computers and smartphones.  
To try the programs and apps at the CryptoParty/PrivacyCafe bring your laptop or smartphone.

# Why?

Privacy is the room in which ideas develop, where you can reflect on those ideas whenever you choose. This room is not only physical, but digital as well. Neither governments nor corporations respect that. But we do.

-- https://www.cryptoparty.in

## Upcoming Cryptoparties @ HSBXL

{{< events when="upcoming" series="Cryptoparty" >}}

## Past Cryptoparties @ HSBXL

{{< events when="past" series="Cryptoparty" >}}
