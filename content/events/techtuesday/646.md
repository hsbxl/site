---
techtuenr: "646"
startdate: 2022-09-13
starttime: 19:00
price: ""
eventtype: Social get-together
eventid: techtue646
series: TechTuesday
title: TechTuesday 646
linktitle: "TechTue 646"
location: HSBXL
image: techtuesday.png
---

Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.
