---
techtuenr: "631"
startdate: 2022-05-31
starttime: 19:00
price: ""
eventtype: Social get-together
eventid: techtue631
series: TechTuesday
title: TechTuesday 631
linktitle: "TechTue 631"
location: HSBXL
image: techtuesday.png
---

Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.
