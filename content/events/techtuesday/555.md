---
eventid: techtue555
startdate: 2020-01-07
starttime: "19:00"
linktitle: "TechTue 555"
title: "TechTuesday 555"
price: ""
image: "techtuesday.jpg"
series: "TechTuesday"
eventtype: "Social get-together"
location: "HSBXL"
---

Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.
