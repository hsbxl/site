---
techtuenr: "620"
startdate: 2022-03-15
starttime: 19:00
price: ""
eventtype: Social get-together
eventid: techtue620
series: TechTuesday
title: TechTuesday 620
linktitle: "TechTue 620"
location: HSBXL
image: techtuesday.png
---

Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.
