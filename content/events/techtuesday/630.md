---
techtuenr: "630"
startdate: 2022-05-24
starttime: 19:00
price: ""
eventtype: Social get-together
eventid: techtue630
series: TechTuesday
title: TechTuesday 630
linktitle: "TechTue 630"
location: HSBXL
image: techtuesday.png
---

Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.
