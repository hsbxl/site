---
techtuenr: "613"
startdate: 2021-08-17
starttime: 19:00
price: ""
eventtype: Social get-together
eventid: techtue613
series: TechTuesday
title: TechTuesday 613
linktitle: "TechTue 613"
location: HSBXL
image: techtuesday.png
---

Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.
