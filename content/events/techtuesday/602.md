---
techtuenr: "602"
startdate: 2021-11-09
starttime: 19:00
price: ""
eventtype: Social get-together
eventid: techtue602
series: TechTuesday
title: TechTuesday 602
linktitle: "TechTue 602"
location: HSBXL
image: techtuesday.png
---

Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.
