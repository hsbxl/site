---
techtuenr: "599"
startdate: 2021-10-19
starttime: 19:00
price: ""
eventtype: Social get-together
eventid: techtue599
series: TechTuesday
title: TechTuesday 599
linktitle: "TechTue 599"
location: HSBXL
image: techtuesday.png
---

Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.
