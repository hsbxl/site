---
techtuenr: "614"
startdate: 2022-02-01
starttime: 19:00
price: ""
eventtype: Social get-together
eventid: techtue614
series: TechTuesday
title: TechTuesday 614
linktitle: "TechTue 614"
location: HSBXL
image: techtuesday.png
---

Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.
