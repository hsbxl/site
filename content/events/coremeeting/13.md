---
startdate: 2019-01-05
starttime: "13:00"
linktitle: "Coremeeting 13"
title: "Coremeeting 13"
location: "HSBXL"
eventtype: "Meeting"
price: ""
series: "coremeeting"
hackeragenda: "false"
---

Our monthly CoreMeeting. Discussing main issues like the bookkeeping.

# Agenda

- Membership price:
  - Mention the membership differently. Instead of 'membership is 25, social fee 15', say 'memership is at least 15, but to break even for our costs, we calculated we need 20 to 25'.
  - Tom will make a first draft
- Address VZW? Needed for statute updates.

  - WSI will ask gerd if it's ok that we recieve mail here
    - If its still a no Vincent will be able to help.
  - https://kbopub.economie.fgov.be/kbopub/zoekadresform.html?postcod1=1070&postgemeente1=1070+-+Anderlecht+%28Anderlecht%29&straatgemeente1=GRONDELSSTRAAT&huisnummer=152&filterEnkelActieve=true&_filterEnkelActieve=on&actionLU=Zoek

- Dealing with the rodent invasion

  - poison and traps
  - tom will get some from the brico
  - keep food locked up in metal containers
  - greg will o look for some metal boxes to keep food in

- stop KBC?

  - Tom wouter and fred need to go to the office at the same time
  - Wouter will ask again for the cancelation papers

- double insurance?

  - To check by Fred

- warranty molenbeek?

  - Wouter will mail sarah to ask for it

- bookkeeping overview

  - current 3341
  - last rent in molenbeek was paid
  - still to pay from the move
    - van was 282 euro
    - there might also be a cost from straps that olivier bought, straps was 80 euro
    - fred put fuel in the van before returing it .. 17 euro

- mails not working:
  - contact@hsbxl.be
  - fred will have a look
- bytenight todos?
  - go to containerpark, empty the "black room"
  - remove motion detection lights
  - make the "common area" more atmosphery
  - get vegetables for soup
  - Ask Gerd if we can use an electric heat cannon during the event
  - make poster
  - put up signage
  - fire for outside in barrels
  - make colruytrun
