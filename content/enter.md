---
title: "How to get to HSBXL?"
linktitle: "How to get to HSBXL?"
---

**Address:** Rue Osseghem 53, 1080 Molenbeek, Brussels, Belgium.
([Open Street Map](https://www.openstreetmap.org/node/7115056369))
([GoogleMaps](https://www.google.com/maps/place/Hackerspace+Brussels+HSBXL/@50.8552895,4.3188495,17z/data=!3m1!4b1!4m6!3m5!1s0x47c3c37ae6875a27:0x962d7fc36e046fa7!8m2!3d50.8552861!4d4.3214244!16s%2Fg%2F12lk5rsf3?entry=ttu))

# We are now located in LionCity

HSBXL has moved to LionCity, a vibrant and innovative space in the heart of Brussels. Situated in the former Delhaize distribution centre in Molenbeek, LionCity is home to a diverse mix of crafts, entrepreneurs, social projects, culture, sports, and urban agriculture.

For more details on LionCity, visit [here](https://www.entrakt.be/en/lioncity).

# How to get to the address

{{< image src="/images/enter_space/hsbxl_lioncity.png" >}}

© [OpenStreetMap contributors](https://www.openstreetmap.org/copyright) ♥

### For assistance getting in

- **Chat**: Join us at [Matrix chat](https://matrix.to/#/#hsbxl:matrix.org). Preferred for writing ahead of time.
- **Phone**: [+32 2 880 40 04](tel:+3228804004). Available when members are present in the space.

## By public transport 🚆

the hackerspace is just a few dozen meter from **metro Beekkant**
Most metrolines in brussels (1,5,2,6) stop at this station

If you are transfering from train to metro it is easiest to do so at the Central station Bruxelles-Centrale / Brussel-Centraal (line 1 to gare de l'Ouest / Weststation or 5 to Erasme / Erasmus) or South station Bruxelles-Midi / Brussel-Zuid (line 2 to Simonis or 6 to Roi Baudouin / Koning Boudewijn). 

It's possible to come from north station Bruxelles-Nord Brussel-Noord, but you will have to walk to Rogier and either take the metro 2 or 6 to Elisabeth and change there or take the same metros as from the South station and pass through there. In most cases this means it's better and quicker to continue your train ride to the Central or South station and take the metro there. 

The nearest train station is West station Gare de l'Ouest / Weststation, where you can take the train towards Stockel / Stokkel to come to the space, but only few trains go there.

Between midnight and 3am on the nights from Friday to Saturday and from Saturday to Sunday, STIB/MIVB runs nightbusses under the banner of Noctis. If you are planning to stay late (for example for Bytenight), please make sure to plan ahead. On top of these night busses, De Lijn also offers the N62 bus which has stops throughout the night until the early moning in Brussels as well as towards the airport. Again planning ahead is advised.

**Instructions once you've arrived at Beekkant**
Upon arrival at Beekkant, you simply exit through the station entrance and you will arrive on a small square with bus stops. Go left at the entrance towards Rue Jules Vieujantstraat, crossing the street. Then continue down Rue Jules Vieujantstraat until you walk onto Rue Osseghemstraat. You will see a closed black gate, while this is a LionCity entrance, it won't lead to the hacker space. Continue to the right down Rue Osseghemstraat until you reach another gate with LionCity banners. This is the entrance to the hackerspace. Please call **please call +32 2 880 40 04** if the gate is closed and someone will come get you. You can follow the instructions below (and see pictures) on how to get into the space itself from the entrance.

## By car 🚘

HSBXL and LionCity don't have any parking spaces available for visitors. You can park in the surrounding streets or in the ParkBee or BePark parking lots close to the space. It's not permitted to park in front of the gate and you will get towed!

## Shared mobility

Brussels has a [vast offering in shared mobility](https://www.brussels.be/shared-mobility). Use a bike, step, or even car using the appropriate plans.

Keep in mind that Brussels has enforced drop zones for shared mobility bikes, electric scooters, and kick scooters (aka trottinette or step). Please park responsibly!

## At night: Collecto

To get back to your home/hotel, there is [Collecto](https://en.collecto.be/). A Taxi picks you up at certain points (many STIB public transport stops) and brings you to your destination within the Brussels region for the fixed price of 6 euro.

You can order a Collecto at any time between 11 p.m. and 6 a.m, but it is advisable to order at least 20 minutes in advance. If you have a smartphone, you can use the [Android](https://play.google.com/store/apps/details?id=be.tradecom.collecto&hl=en&gl=US) or [Apple iOS](https://apps.apple.com/be/app/collecto-your-shared-taxi/id921558166) app, otherwise, you can call +32 2 800 36 36 to reserve your Collecto.

There is a [collecto](https://en.taxisverts.be/collecto) startpoint at the entrance of Beekkant STIB metro station, at 100m away

# How to get to the Hackerspace once you have reached LionCity

First of all make sure you're at the right entrance for LionCity. This is the gate at Rue Osseghemstraat 53 at the intersection of Rue Osseghemstraat and Rue Jules Delhaizestraat. **The right gate is a barred gate with big banners with LionCity** on them. If you're at a black gate made of metal panels, you're at the wrong gate and should turn right. 

{{< image src="/images/enter_space/wrong_entrance.jpg" >}}
{{< image src="/images/enter_space/right_entrance.jpg" >}}

If the gate is closed **please call +32 2 880 40 04** so someone can come help you get in. If the gate is open, you can walk in and continue until you're on the roofed street. The entrance to the hackerspace is the first door on the right. There is a doorbell on the right side of the door. 

{{< image src="/images/enter_space/front-of-building.jpg" >}}

---
