---
title: "Mailing lists"
linktitle: "Mailing list"
---

# General public mailing list.

Our HSBXL public mailing list (mailman).

- Not publicly archived.
- The same patterns apply on the mailing list as in the space: https://wiki.hsbxl.be/Patterns

https://lists.hsbxl.be/listinfo/hsbxl

# 42 mailing list, intra-hackerspaces.

For Belgian events/workshops/tech-talk we have the [42]. Belgian spaces mailing list.
https://discuss.hackerspaces.be/mailman3/lists/42.discuss.hackerspaces.be/

# The first history, archived.

For the history, the code31 mailing list was used/hijacked as the first HSBXL mailing list around the year 2008.
http://web.archive.org/web/20100706005557/http://code31.lahaag.org/pipermail/code31
