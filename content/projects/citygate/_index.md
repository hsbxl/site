---
title: "Move to Citygate"
linktitle: "Move to Citygate"
state: running
---

The content has been moved to [Nextcloud](https://cloud.hsbxl.be/index.php/s/xqzsoGCRWmHLA4r?path=%2Fcitygate) in order to make the website more lightweight.
