---
title: "GNUwhine"
linktitle: "GNUwhine"
state: archived
image: "gnuwhine.png"
---

# Ingredients AKA 'the source'

for 10 liter HSBXL GNUwhine

- 7L red wine
- 1L white wine
- 2.5L brown rum (0.5L for flambing the sugar)
- 1.5Kg sugar
- oranges, lemons, grapefruit
- cinnamon
- clove

# Instructions

- https://www.youtube.com/watch?v=wxXm-0yFLeg
- https://www.youtube.com/watch?v=XjSPgmZ_mx0
