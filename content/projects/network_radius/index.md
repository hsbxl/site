---
title: "Network/RADIUS"
linktitle: "Network/RADIUS"
date: 2018-11-06T22:08:31+01:00
state: running
maintainer: "askarel"
---

**_ WORK IN PROGRESS _**

This article describe HSBXL RADIUS server setup.

We will be using FreeRADIUS 3.0.

# Wanted features

- RADIUS authentication on switches
- 802.1x authentication on access points
- Roaming (SpaceFED/EduRoam)
- LDAP user backend
- Dynamic VLAN assignment on WiFi
- RADIUS authentication on wiki (later)

# LDAP setup

We need a back-end that contains a list of users with their passwords. You can choose to use the file /etc/freeradius/3.0/users instead of a full LDAP installation for small configurations (1-3 users), but you will feel the administrative pain very quickly. Do yourself a favour and save yourself from unneeded pain: use LDAP.

Since our LDAP server does not allow to be queried as anonymous, the FreeRADIUS server will need credentials to connect to the LDAP server and do some queries

## Generate a password for the FreeRADIUS user

```
 # pwgen 30 1
 baigah2cai6chaa5Osh1en3Ahde4Je
 # /usr/sbin/slappasswd -s baigah2cai6chaa5Osh1en3Ahde4Je
 {SSHA}l0uiXg0djOmSAf3SIcN44f7muWrLPpOt
```

## Create a LDAP user for FreeRADIUS

```
dn: uid=FreeRADIUS,ou=services,dc=hsbxl,dc=be
objectclass: account
objectclass: simpleSecurityObject
objectclass: top
uid: FreeRADIUS
userpassword: {SSHA}l0uiXg0djOmSAf3SIcN44f7muWrLPpOt
```

## Configure FreeRADIUS to use LDAP

File /etc/freeradius/3.0/mods-enabled/ldap

```
ldap {
    ...
	identity = 'uid=FreeRADIUS,ou=services,dc=hsbxl,dc=be'
	password = baigah2cai6chaa5Osh1en3Ahde4Je
	base_dn = 'dc=hsbxl,dc=be'
    ...
	group {
	    ...
	    filter = '(objectClass=groupOfNames)'
	    ...
	    membership_filter = "(|(member=%{control:Ldap-UserDn})(memberUid=%{%{Stripped-User-Name}:-%{User-Name}}))"
	    ...
	}
    ...
}
```

## Test the setup

Once you got LDAP configured, you need to test if it is working correctly

```

```

If the test above succeeds, we can go to the next step below:

# Clients

The clients are the devices that will contact the RADIUS server for authentication. Clients can be any of the following:

- a switch
- a firewall
- an access point
- an application (web applications included)
- another RADIUS server acting as a proxy (think spaceFED)

## Setting up the first client

```

```

# Advanced configurations

## Cisco switch

```

```

## Juniper switch/firewall

```

```

## Unifi access points

```

```
